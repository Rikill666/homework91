const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const messageScheme = new Scheme({
    user:{
        type: Scheme.Types.ObjectId,
        ref: 'User',
        required: true
    },
    text: {
        type: String,
        required:true
    },
    datetime:{
        type: Date,
        required: true,
        default: new Date()
    }
});

const Message = mongoose.model("Message", messageScheme);
module.exports = Message;
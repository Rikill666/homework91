const express = require('express');
const cors = require('cors');
const expressWs = require('express-ws');
const Message = require('./models/Message');
const users = require('./app/users');
const mongoose = require("mongoose");
const config = require('./config');
const User = require('./models/User');


let messages = [];
const connections = {};

const app = express();
app.use(cors());
const port = 8000;
expressWs(app);
app.use(express.json());

app.ws('/chat', async function (ws, req) {

    const user = await User.findOne({token: req.query.token});
    connections[user.username] = ws;

    try {
        messages = await Message.find().sort('-datetime').limit(30).populate("user");
    } catch (error) {
        console.log(error);
    }
    ws.send(JSON.stringify({
        type: 'USERS_AND_MESSAGES',
        messages: messages,
        usersNames: Object.keys(connections)
    }));

    Object.keys(connections).forEach(connId => {
        const connection = connections[connId];
        if (!connections[user.username]) {
            connection.send(JSON.stringify({
                type: 'NEW_USER',
                user: user.username
            }));
        }
    });

    ws.on('message', async (msg) => {
        console.log(`Incoming message from ${user.username}: `, msg);

        const parsed = JSON.parse(msg);

        switch (parsed.type) {
            case 'CREATE_MESSAGE':
                let message = null;
                let user = null;
                try {
                    user = await User.findOne({username: parsed.user});
                    const newMessage = {text: parsed.text, user: user._id};
                    message = new Message(newMessage);
                    await message.save();
                } catch (error) {
                    console.log(error);
                }

                Object.keys(connections).forEach(connId => {
                    const connection = connections[connId];

                    connection.send(JSON.stringify({
                        type: 'NEW_MESSAGE',
                        _id: message._id,
                        text: message.text,
                        user: user
                    }));
                    messages.push(message);
                });
                break;
            default:
                console.log('NO TYPE: ' + parsed.type);
        }
    });

    ws.on('close', (msg) => {
        console.log(`client disconnected! ${user.username}`);
        delete connections[user.username];
        Object.keys(connections).forEach(connId => {
            const connection = connections[connId];
            if (!connections[user.username]) {
                connection.send(JSON.stringify({
                    type: 'DELETE_USER',
                    user: user.username
                }));
            }
        });
    });
});

const run = async () => {
    await mongoose.connect(config.database, config.databaseOptions);
    app.use('/users', users);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
};
run().catch(e => {
    console.log(e);
});



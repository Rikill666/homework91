const User = require('../models/User');

const auth = async (req, res, next) => {
    console.log(req.query.token);
    const token = req.query.token;
    console.log(token + "111111111111111111111111111");
    if(!token){
        return res.status(401).send({error: "Access is denied!!!"});
    }

    const user = await User.findOne({token: token});
    if(!user){
        return res.status(401).send({error: "Access is denied!!!"});
    }
    req.user = user;
    next();
};
module.exports = auth;
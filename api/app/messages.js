const express = require('express');
const permit = require('../middleware/permit');
const Message = require('../models/Message');
const router = express.Router();
const auth = require('../middleware/auth');



router.get("/", async (req, res) => {
    try {
        const messages = await Message.find().limit(30).sort('datetime').populate("user");
        res.send(messages);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/', auth, async (req, res) => {
    const user = req.user;
    const newMessage = req.body;
    newMessage.user = user._id;
    try {
        const message = new Message(newMessage);
        await message.save();
        res.send(message);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const message = await Message.findOne({_id: id});
        if (!message) {
            return res.status(404).send("No this message");
        }
        await Message.deleteOne({_id: id});
        res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;
import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import {connect} from "react-redux";

class Chat extends Component {
    state = {
        text: '',
        messages: [],
        users: []
    };

    componentDidMount() {
        this.websocket = new WebSocket('ws://localhost:8000/chat?token=' + this.props.user.token);

        this.websocket.onmessage = (message) => {
            try {
                const data = JSON.parse(message.data);

                if (data.type === 'NEW_MESSAGE') {
                    const newMessage = {
                        text: data.text,
                        _id: data._id,
                        user: data.user
                    };
                    const messages = [...this.state.messages];
                    messages.unshift(newMessage);
                    this.setState({messages})
                } else if (data.type === 'USERS_AND_MESSAGES') {
                    this.setState({messages: data.messages, users: data.usersNames});
                } else if (data.type === 'NEW_USER') {
                    this.setState({users: [...this.state.users, data.user]})
                } else if (data.type === 'DELETE_USER') {
                    const users = [...this.state.users];
                    const index = users.findIndex(u => u === data.user);
                    delete users[index];
                    this.setState({users})
                }
            } catch (e) {
                console.log('Something went wrong', e);
            }
        };

    }

    sendMessage = e => {
        e.preventDefault();

        const message = {
            type: 'CREATE_MESSAGE',
            text: this.state.text,
            user: this.props.user.username
        };

        this.websocket.send(JSON.stringify(message));
        this.setState({text: ""});
    };
    changeField = e => this.setState({[e.target.name]: e.target.value});

    render() {
        return (
            <>
                <Row>
                    <Col sm={12} md={4} lg={3}>
                        {this.state.users.map((user, i) => (
                            <div key={user}>
                                <strong>{user}</strong>
                            </div>
                        ))}
                    </Col>
                    <Col sm={12} md={8} lg={9}>
                        <form onSubmit={this.sendMessage}>
                            <input type="text" value={this.state.text} name="text" onChange={this.changeField}/>
                            <button type="submit">Send message!</button>
                        </form>

                        {this.state.messages.map((msg, i) => (
                            <div key={msg._id}>
                                <strong>{msg.user.username}: </strong>{msg.text}
                            </div>
                        ))}
                    </Col>
                </Row>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user
    };
};

export default connect(mapStateToProps)(Chat);
import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

import {useSelector} from "react-redux";
import Chat from "./containers/Chat/Chat";

const ProtectedRoute = ({isAllowed, ...props})=>(
    isAllowed ? <Route {...props}/>:<Redirect to="/login"/>
);

const Routes = () => {
    const user = useSelector(state=>state.users.user);
    return (
        <Switch>
            <ProtectedRoute isAllowed={user} path="/" exact component={Chat}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route render={() => <h1>Not Found</h1>}/>
        </Switch>
    );
};

export default Routes;